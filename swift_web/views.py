from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.core.servers.basehttp import FileWrapper  
from django.conf import settings

import swiftclient
from swiftclient import Connection as swift_connection
import logging
import logging.config
import sys, os
import urllib2

path = os.path.dirname(__file__)
logging.config.fileConfig("%s/logging.conf" % path)

server_ip = settings.PROXY_IP

def home(request):
    if not __isLogin(request):return HttpResponseRedirect(reverse('views.login'))
    return list_containers(request)

def login(request):
    ''' user login..'''
    if __isLogin(request):return HttpResponseRedirect(reverse('views.home'))
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        __getLogger().info("username: %s ; password: %s" % (username, password))
        try:
            (url, token) = swiftclient.get_auth("%s/auth/v1.0" % server_ip,
                               username, password)
            request.session['url'] = url
            request.session['token'] = token
            request.session['username'] = username
            request.session['password'] = password
            request.session.set_expiry(0)
            return HttpResponseRedirect(reverse('views.home'))
        except Exception, e:
            print e
            login_msg = 'username or password is wrong...'
    return render_to_response('login.html', locals())

def logout(request):
    request.session.pop('url')
    request.session.pop('token')
    request.session.pop('username')
    request.session.pop('password')
    return  HttpResponseRedirect(reverse('views.login'))


def __init_swift_conn(request):
    ''' init the swift_connetion 
    para request: HTTP_Request
    return conn :swift_conn
    return url : auth_url
    return token :auth_token
    '''
    url = request.session.get('url')
    token = request.session.get('token')
    username = __getUserName(request)
    password = __getPassword(request)
    #TODO check url
    url = url.replace('http://127.0.0.1:8080', server_ip)
    conn = swift_connection(server_ip, username, password, preauthurl=url, preauthtoken=token)
    return conn, url, token

def list_containers(request):
    conn, url, token = __init_swift_conn(request)
    (headers, containers) = conn.get_account()
    return render_to_response("containers.html", locals())

def list_objects(request, container):
    p = request.GET.get('p')
    conn, url, token = __init_swift_conn(request)
    (headers, objects) = conn.get_container(container,delimiter='/',qpath=p)
    objects = [obj for obj in objects if 'subdir' not in obj]
    # this is to exclude the subdir nodes like {"subdir":"test1/"}
    # which will influence the UI
    return render_to_response("objects.html", locals())

def get_object(request, container, object):
    conn, url, token = __init_swift_conn(request)
    (headers , content) = conn.get_object(container, object)
    response = HttpResponse(content, mimetype=headers.get('content-type')) 
    response['Content-Disposition'] = 'attachment; filename=%s' % urllib2.quote(object.encode('gbk','ignore'))
    return response  

def create(request, container=None):
    '''create a container'''
    p = request.GET.get('p')
    if request.method == "POST":
        conn, url, token = __init_swift_conn(request)
        containerName = request.POST.get('c')
        try:
            if container: #Pseudo-Hierarchical Folders
                if p:
                    containerName = p+"/"+containerName
                conn.put_object(container,containerName,None,content_type='application/directory')
            else:
                print containerName
                conn.put_container(containerName)
            msg = "create folder ok."
            pos = -2
            return render_to_response('msg.html',locals())
        except:
            msg = "create folder faild."
            pos = -2
            return render_to_response('msg.html',locals())
            
    return render_to_response('create.html', locals())

def upload(request, container):
    ''' upload a object '''
    p = request.GET.get('p')
    if request.method == "POST" :
        if request.FILES.get('object', None):
            obj = request.FILES.get('object', None)
            conn, url, token = __init_swift_conn(request)
            path = request.GET.get('p')
            filename = obj.name
            if path:
                filename = path+"/"+filename
            print filename
            try:
                conn.put_object(container, filename, obj)
                msg = "upload ok.."
                pos = -2
            except:
                msg = "upload failed.."
                pos = -2
            return render_to_response('msg.html',locals())
    return render_to_response('upload.html', locals())

def delete_object(request, container, object):
    ''' delete a object '''
    conn, url, token = __init_swift_conn(request)
    try:
        conn.delete_object(container, object)
    except:
        pass
    return HttpResponseRedirect(reverse('views.home'))

def delete_container(request, container):
    ''' delete a container '''
    conn, url, token = __init_swift_conn(request)
    try:
        conn.delete_container(container)
    except:
        pass
    return  HttpResponseRedirect(reverse('views.home'))

def edit_object(request, container, object):
    '''
    Edit the object metaData 
    '''
    metastr = 'x-object-meta-'
    conn, url, token = __init_swift_conn(request)
    if request.method == "POST":
        #update meta data
        metas = {}
        keys = request.POST.getlist('meta-key')
        values = request.POST.getlist('meta-value')
        for i in range(0,len(keys)):
            if keys[i] and values[i]:
                metas[metastr+keys[i]] = values[i]
                conn.post_object(container, object, metas)
    headers = conn.head_object(container, object)
    metas = [dict(key=k.lstrip(metastr),value=headers[k]) for k in headers.keys()\
              if k.find(metastr)!= -1]
    return render_to_response('edit.html', locals())

def __isLogin(request):
    ''' check if user is login'''
    if request.session.get('token')is not None and request.session.get('url') is not None:
        return True
    else : return False
    
def __getLogger():
    return logging.getLogger(sys.argv[0])

def __getUserName(request):
    return request.session.get('username')

def __getPassword(request):
    return request.session.get('password') 
