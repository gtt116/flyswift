from django.conf.urls.defaults import *
from django.conf import settings
import views
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
     (r'^admin/', include(admin.site.urls)),
     (r'^media/(?P<path>.*)$','django.views.static.serve',
        {'document_root': settings.MEDIA_PATH}),
)
urlpatterns += patterns('views',
     (r'^$', "home"),
     (r'^login/', "login"),
     (r'^logout/', "logout"),
     (r'^home/$', "home"),
     (r'^create/$', "create"),
     (r'^home/(?P<container>\w+)/create/$', "create"),
     (r'^home/(?P<container>\w+)/delete/$', "delete_container"),
     (r'^home/(?P<container>\w+)/upload/$', "upload"),
     (r'^home/(?P<container>\w+)/$', "list_objects"),
     (r'^home/(?P<container>\w+)/(?P<object>.*)/edit/$', "edit_object" ),
     (r'^home/(?P<container>\w+)/(?P<object>.*)/delete/$', "delete_object" ),
     (r'^home/(?P<container>\w+)/(?P<object>.*)/$', "get_object" ),
)

