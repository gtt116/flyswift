from django import template

register = template.Library()

@register.filter
def baseName(value):
    return value.split("/")[-1]

