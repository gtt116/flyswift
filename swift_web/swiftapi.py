import pycurl
import simplejson
import os
import urllib2

class HTTPConnection:
	'''
	a http connection..
	'''
	def __init__(self, url, method='GET', header=[], upload=None):
		'''
		 para url: the url to read
		 para method: the requet method
		 para header: http headers to add
		 para upload: the file to put
		'''
		self.resHeaders = {}
		self.resBody = ''
		self.resCode = 0
		self.reason = ''
		self.uploadMd5 = ''
		self.curl = pycurl.Curl()
		if type(url) == unicode: url = url.encode('gbk', 'ignore')
		self.curl.setopt(pycurl.URL, url)
		if upload is not None: # is put method
			self.curl.setopt(pycurl.UPLOAD, 1)
			self.curl.setopt(pycurl.READFUNCTION, upload.read)
			self.curl.setopt(pycurl.HTTPHEADER, header)
			header.append('Transfer-Encoding: chunked')
#			self.curl.setopt(pycurl.READDATA, upload)
		else:					#is get/head method
			self.curl.setopt(pycurl.HTTPHEADER, header)
			self.curl.setopt(pycurl.CUSTOMREQUEST, method.upper())
		self.curl.setopt(pycurl.WRITEFUNCTION, self.__getBody__)
		self.curl.setopt(pycurl.HEADERFUNCTION, self.__getHeader__)
		
	def __getHeader__(self, headcontent):
		'''
		Parse response header
		'''
		if headcontent.find('HTTP/1.1') != -1:
			firstLine = headcontent.split()
			self.reason, self.resCode = firstLine[2], firstLine[1]
		else:
			try:
				(key, value) = headcontent.split(": ")
				self.resHeaders[key.strip()] = value.strip()
			except:pass
		
	def __getBody__(self, bodyContent):
		self.resBody += bodyContent
		
	def perform(self):
		'''
		Let pycurl to send the request
		'''
		self.curl.perform()
		
	def getHeader(self, head):
		'''
		Get the response header 
		para head: the key of response head
		'''
		return self.resHeaders.get(head)
		
	def getHeaders(self):
		'''
		Get all the response headers
		'''
		return self.resHeaders
		
	def setHeader(self, head):
		'''
		Set the request header
		'''
		head = [str(h) for h in head]
		self.curl.setopt(pycurl.HTTPHEADER, head)
		
	def setRequestMethod(self, method):
		'''
		Set the request method,ie POST,HEAD
		'''
		self.curl.setopt(pycurl.CUSTOMREQUEST, method.upper())
		
	def getBody(self):
		'''
		Get the request body
		'''
		return self.resBody
		
	def state(self):
		'''
		Get the response state code
		'''
		return int(self.resCode)
		
	def reson(self):
		return self.reason

		
# -------------------
def getAuthToken(url, username, password):
	'''
	Get the X-Storage-Url and X-Auth-Token
	
	param url: storage URL
	param username:  user to authenticate as
	param key: key or password for authorization
	returns: tuple of (storage URL, auth token)
	'''
	conn = HTTPConnection(url)
	header = ['x-storage-user: %s' % username,
				'x-storage-pass: %s' % password]
	conn.setHeader(header)
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception("get auth token failed.")
	storageUrl = conn.getHeader('X-Storage-Url')
	authToken = conn.getHeader('X-Auth-Token')
	return (storageUrl, authToken)
	
# ----------Account operations------#
def getAccount(url, authToken , marker=None, limit=None, prefix=None):
	'''
	Get a listing of containers for the account.
	
	:param url: storage URL
    :param authToken: auth token
    :param marker: marker query
    :param limit: limit query
    :param prefix: prefix query
	:returns: a tuple of (response headers, a list of containers) The response
              headers will be a dict .
	'''
	url = "%s?format=json" % url
	conn = HTTPConnection(url)
	header = ['X-Auth-Token: %s' % authToken]
	conn.setHeader(header)
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Account GET failed.')
	return conn.getHeaders(), simplejson.loads(conn.getBody())
	
def headAccount(url, authToken):
	'''
    Get account stats.

    :param url: storage URL
    :param token: authToken token
    :returns: a dict containing the response's headers (all header names will
              be lowercase)
    '''
	url = "%s?format=json" % url
	conn = HTTPConnection(url)
	header = ['X-Auth-Token: %s' % authToken]
	conn.setHeader(header)
	conn.setRequestMethod("HEAD")
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Account HEAD failed.')
	return conn.getHeaders()
	
def postAccount(url, authToken, headers={}):
	'''
	Update an account's metadata.
	
	:param url: storage URL
    :param token: auth token
    :param headers: additional headers to include in the request
    :raises Exception: HTTP POST request failed
	'''
	headers['X-Auth-Token'] = authToken
	conn = HTTPConnection(url)
	conn.setHeader(__d2l(headers))
	conn.setRequestMethod('POST')
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Account POST failed.')

# ----------Container operations--------#
def getContainer(url, authToken, container, marker=None, limit=None, prefix=None):
	'''
    Get a listing of objects for the container.

    :param url: storage URL
    :param authToken: auth token
    :param container: container name to get a listing for
    :param marker: marker query
    :param limit: limit query
    :param prefix: prefix query
    :param delimeter: string to delimit the queries on
    :returns: a tuple of (response headers, a list of objects) The response
              headers will be a dict and all header names will be lowercase.
    :raises ClientException: HTTP GET request failed
    '''
	url = url.rstrip('/')
	url = '%s/%s?format=json' % (url, container)
	header = ['X-Auth-Token: %s' % authToken]
	conn = HTTPConnection(url, header=header)
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Container Get failed.')
	return conn.getHeaders(), simplejson.loads(conn.getBody())
	
def putContainer(url, authToken, container, headers={}):
	'''
    Create a container

    :param url: storage URL
    :param token: auth token
    :param container: container name to create
    :param headers: additional headers to include in the request
    :raises ClientException: HTTP PUT request failed
    '''
    
	url = url.rstrip('/')
	url = '%s/%s' % (url, container)
	headers['X-Auth-Token'] = authToken
	conn = HTTPConnection(url, header=__d2l(headers), method='PUT')
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Container put failed:%s' % container)

def headContainer(url, authToken, container, marker=None, limit=None, prefix=None):
	'''
    Get container stats.

    :param url: storage URL
    :param token: auth token
    :param container: container name to get stats for
    :param http_conn: HTTP connection object (If None, it will create the
                      conn object)
    :returns: a dict containing the response's headers (all header names will
              be lowercase)
    :raises ClientException: HTTP HEAD request failed
    '''
	url = url.rstrip('/')
	url = '%s/%s?format=json' % (url, container)
	header = ['X-Auth-Token: %s' % authToken]
	conn = HTTPConnection(url, header=header, method="HEAD")
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Container Get failed.')
	return conn.getHeaders()
	
def postContainer(url, token, container, headers={}):
	'''
    Update a container's metadata.

    :param url: storage URL
    :param token: auth token
    :param container: container name to update
    :param headers: additional headers to include in the request
    :param http_conn: HTTP connection object (If None, it will create the
                      conn object)
    :raises ClientException: HTTP POST request failed
    '''
	url = url.rstrip('/')
	url = '%s/%s' % (url, container)
	headers['X-Auth-Token'] = authToken
	conn = HTTPConnection(url, header=__d2l(headers), method='POST')
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Container put failed:%s')

def deleteContainer(url, token, container, headers={}):
	url = url.rstrip('/')
	url = '%s/%s' % (url, container)
	headers['X-Auth-Token'] = token
	conn = HTTPConnection(url, header=__d2l(headers), method='DELETE')
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Container DELETE failed:\n%s:%s:%s' 
						% (conn.state(),conn.reson(),conn.getBody()))
		
# -------- Object operations -------- #
def getObject(url, authToken, container, object, marker=None, limit=None, prefix=None):
	'''
    Get an object

    :param url: storage URL
    :param token: auth token
    :param container: container name that the object is in
    :param object: object name to get
    :returns: a tuple of (response headers, the object's contents) The response
		headers will be a dict and all header names will be lowercase.
    :raises ClientException: HTTP GET request failed
    '''
	url = url.rstrip('/')
	url = '%s/%s/%s?format=json' % (url, urllib2.quote(container), urllib2.quote(object))
	header = ['X-Auth-Token: %s' % authToken]
	conn = HTTPConnection(url, header=header)
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Container Get failed.')
	return conn.getHeaders(), conn.getBody()
	
def putObject(url, authToken, container, object, headers={}, etag=None):
	'''
	 Put an object

    :param url: storage URL
    :param token: auth token; if None, no token will be sent
    :param container: container name that the object is in; if None, the
                      container name is expected to be part of the url
    :param object: object name to put; if None, the object name is expected to be
                 part of the url
    :param etag: etag of contents; if None, no etag will be sent
    :param chunk_size: chunk size of data to write; default 65536
    :param content_type: value to send as content-type header; if None, no
                         content-type will be set (remote end will likely try
                         to auto-detect it)
    :param headers: additional headers to include in the request, if any
    :returns: etag from server response
    :raises ClientException: HTTP PUT request failed
    '''
	url = url.rstrip('/')
	url = '%s/%s/%s' % (url, urllib2.quote(container), urllib2.quote(os.path.basename(object.name)))
	headers['X-Auth-Token'] = authToken
	if etag: headers['ETag'] = etag.strip()
	conn = HTTPConnection(url, header=__d2l(headers), upload=object)
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Container Get failed.')
	return conn.getHeader("ETag")
	
def headObject():
	pass
	
def postObject():
	pass

def deleteObject(url, token, container, objName):
	url = url.rstrip('/')
	url = '%s/%s/%s' % (url, urllib2.quote(container), urllib2.quote(objName))
	header = ['X-Auth-Token: %s' % token]
	conn = HTTPConnection(url, header=header, method="DELETE")
	conn.perform()
	if conn.state() < 200 or conn.state() >= 300:
		raise Exception('Container DELETE failed. %s' % conn.state())
	return conn.getHeaders(), conn.getBody()	
	
	
def printDict(dict):
	for (key, value) in dict.iteritems():
		print "%s: %s" % (key, value)
	
def __d2l(dict):
	''' convert dict to list'''
	list = []
	for (key,value ) in dict.iteritems():
		list.append("%s: %s" %(key,value) )
	return list

def __l2d(list):
	''' convert list to dict'''
	dict = {}
	for l in list:
		(key,value) = l.split(": ")
		dict[key] = value
	return dict

if __name__ == "__main__":
	(url, authToken) = getAuthToken('http://127.0.0.1:8080/auth/v1.0', 'test:tester', 'testing')
#	url = 'http://192.168.59.128:8080/v1/AUTH_68a98e0a-9245-4158-a1ed-a9d3a9a969e4'
#	f = file('e:/linux-1.1.13.tar.bz2')
#	putObject(url, authToken, "dir3", f)
	# putContainer(url , authToken , "dir3")
	# printDict(getAccount(url,authToken)[1][2])
	# printDict(headContainer(url,authToken, 'dir3'))
#	print getContainer(url, authToken, 'dir3')
	# print getContainer(url , authToken , 'dir2')
	# print getObject(url, authToken , 'dir1', 'getauthtoken.sh')[1]
	
